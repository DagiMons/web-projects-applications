#Система бронирования мест на парковках СПб "Парктроник"
##Автор
Позмогов Кирилл, группа P3220
##Описание
Данный сервис должен помочь автомобилистам нашего с вами города с довольно сложной задачей-нахождением парковочного места на его просторах. У приложения будет 3 группы пользователей: 
###
Незарегистрированные гости, которые могут увидеть список парковок, сотрудничающих с данным сайтом, свободные места на них(если таковые имеются), рекламу и ссылку на онлайн-карты(яндекс или гугл карты)
###
Зарегистрированные водители, которые могут бронировать свободные места(1 место на аккаунт) на парковках помимо всего того, что могут гости.
###
Владельцы парковок, сотрудничающих с сайтом, которые могут все вышеперечисленное, а также бронировать места без ограничений, отменять бронь, лишать водителей возможности бронировать места НА СВОИХ ПАРКОВКАХ с обязательным их уведомлением через встроенную функцию. 
##Детали
Продукт - веб-приложение с базой данных, пользовательсикй интерфейс - веб-страницы.